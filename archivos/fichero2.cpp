#include <iostream>
#include <stdlib.h>
#include <fstream>  
using namespace std;

void escribir();

int main(){
    escribir();
    return 0;
}

void escribir(){
    ofstream archivo;
    string nombreArchivo,frase;
    char rpt;

    cout<<"Ingrese nombre del archivo: "; getline(cin,nombreArchivo);
    archivo.open(nombreArchivo.c_str(),ios::out);

    if (archivo.fail()){
        cout<<"No se pudo abrir el archivo"<<endl;
        exit(1);
    }
    cin.clear();
    do
    {
        
        cout<<"Digite una frase: "; getline(cin,frase);
        archivo<<frase;

        cout<<"Desea agregar otra frase (S/N)"; cin>>rpt;
        cin.ignore();       //limpiamos el buffer para que no haya problemas
    } while ((rpt=='S') || (rpt=='s'));

    archivo.close();
    
}
