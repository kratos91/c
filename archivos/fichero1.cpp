//Escribir en un archivo
#include <iostream>
#include <stdlib.h>
#include <fstream>

using namespace std;

void escribir();

int main(){
    escribir();
    return 0;
}

void escribir(){
    ofstream archivo;
    string nombre,frase;

    cout<<"Digite el nombre del archivo: ";
    getline(cin,nombre);

    archivo.open(nombre.c_str(),ios::out);  //abriendo el archivo,si existe lo reemplaza y si no lo crea
    if (archivo.fail()){
        cout<<"Ocurrio un error al abrir el archivo"<<endl;
        exit(1);
    }

    cout<<"Ingrese un texto: ";
    getline(cin,frase);

    archivo<<frase.c_str();
    archivo.close();
    
}