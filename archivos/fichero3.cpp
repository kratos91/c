#include <iostream>
#include <stdlib.h>
#include <fstream>  
using namespace std;

void lectura();

int main(){
    lectura();
    return 0;
}

void lectura(){
    ifstream archivo;
    string nombreArchivo,texto;

    cout<<"Ingrese nombre del archivo: "; getline(cin,nombreArchivo);
    archivo.open(nombreArchivo.c_str(),ios::in);    //Abrimos el archivo en modo lectura

    if (archivo.fail()){
        cout<<"No se pudo abrir el archivo"<<endl;
        exit(1);
    }

    while (!archivo.eof()){         //mientras no sea el final del archivo
        getline(archivo,texto);
        cout<<texto<<endl;
    }

    archivo.close();    //cerramos el archivo
}