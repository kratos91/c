//Añadir texto al final de un archivo
#include <iostream>
#include <stdlib.h>
#include <fstream>  

using namespace std;

void aniadir();

int main(){
    aniadir();
    return 0;
}

void aniadir(){
    string frase;
    ofstream archivo;
    archivo.open("test.txt",ios::app);  //abrimos el archivo en modo añadir

    if (archivo.fail()){
        cout<<"No se pudo abrir el archivo"<<endl;
        exit(1);
    }

    cout<<"Ingrese el texto que quiera añadir: "; getline(cin,frase);
    archivo<<"\n"<<frase;

    archivo.close();    
}