//Funciones de poscionamiento de escritura y lectura
#include <iostream>
#include <stdlib.h>
#include <fstream>

using namespace std;

void escribir();
void leer();

int main(){
    escribir();
    leer();
    return 0;
}

void escribir(){
    ofstream archivo;
    archivo.open("Posicion.txt",ios::out);

    if(archivo.fail()){
        cout<<"No se pudo abrir el archivo"<<endl;
        exit(1);
    }
    //Funcion tellp para mostrar la poscion actual del cursor en el archivo
    cout<<"Posicion actual: "<<archivo.tellp()<<endl;
    archivo<<"Hola que tal?";
    archivo.seekp(5);
    archivo<<"Como estas?";
    cout<<"Posicion actual: "<<archivo.tellp()<<endl;

    archivo.close();
}

void leer(){
    ifstream archivo;
    archivo.open("Posicion.txt",ios::in);

    if(archivo.fail()){
        cout<<"No se pudo abrir el archivo"<<endl;
        exit(1);
    }

    string linea;
    //para iniciar desde la posición 6
    archivo.seekg(5);
    cout<<"Posicion actual: "<<archivo.tellg()<<endl;
    getline(archivo,linea);
    cout<<linea<<endl;
    archivo.close();
}