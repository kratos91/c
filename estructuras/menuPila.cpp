#include <iostream>
#include <stdlib.h>

using namespace std;

struct Nodo
{
    char dato;
    Nodo *siguiente;
};
//Prototipo de funciones
void agregarNodo(Nodo *&,char);
void eliminarNodo(Nodo *&,char &);

int main(){
    Nodo *pila=NULL;
    int opcion;
    char character;
    do
    {
        cout<<"\tMENU\t"<<endl;
        cout<<"1-Insertar elemento en la pila"<<endl;
        cout<<"2-Mostrar elementos de la pila"<<endl;
        cout<<"3-Salir"<<endl;
        cout<<"Opcion: ";cin>>opcion;

        switch (opcion)
        {
        case 1:
            cout<<"\nAgregando Nodo..."<<endl;
            cout<<"Inserte caracter: ";cin>>character;

            agregarNodo(pila,character);
            break;
        
        case 2:
            cout<<"\nSacando los elementos de la pila "<<endl;
            while(pila!=NULL){
                eliminarNodo(pila,character);
                if(pila!=NULL){
                    cout<<character<<" , ";
                }else{
                    cout<<character<<" ."<<endl;
                }
            }
            break;
        case 3:
            break;
        }
    } while (opcion>0 && opcion<3);
    
    return 0;
}

void agregarNodo(Nodo *&pila,char c){
    Nodo *nuevo_nodo=new Nodo();
    nuevo_nodo->dato=c;
    nuevo_nodo->siguiente=pila;
    pila=nuevo_nodo;
}

void eliminarNodo(Nodo *&pila,char &c){
    Nodo *aux=pila;
    c=aux->dato;
    pila=aux->siguiente;
    delete aux;
}