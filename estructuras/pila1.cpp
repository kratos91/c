//pila

#include <iostream>
#include <stdlib.h>

using namespace std;
struct Nodo
{
    int dato;
    Nodo *siguiente;
};

void agregarNodo(Nodo *&,int); //prototipo para insertar un nodo
void eliminarNodo(Nodo *&,int &);

int main(){
    Nodo *pila=NULL;  //al empezar el puntero va apuntar a NULL
    int n1,data;
    cout<<"Ingrese el numero de datos para agregar:"; cin>>n1;
    for(int i=0;i<n1;i++){
        cout<<"Digite un nuevo número...";
        cin>>data;

        agregarNodo(pila,data);
    }

    cout<<"Sacando los elementos de la pila "<<endl;
    while(pila!=NULL){
        eliminarNodo(pila,data);
        if(pila!=NULL){
            cout<<data<<" , ";
        }else{
            cout<<data<<" ."<<endl;
        }
    }
    return 0;
}

void agregarNodo(Nodo *&pila,int n){
    Nodo *nuevo_nodo=new Nodo();
    nuevo_nodo->dato=n;
    nuevo_nodo->siguiente=pila;
    pila=nuevo_nodo;
    
    cout<<"\nElemento agregado a la pila "<<n<<endl;
}

void eliminarNodo(Nodo *&pila,int &n){
    Nodo *aux=pila;
    n=aux->dato;
    pila=aux->siguiente;
    delete aux;
}