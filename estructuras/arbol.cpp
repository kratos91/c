#include <iostream>
#include <stdlib.h>

using namespace std;

struct Nodo
{
    int dato;
    Nodo *der;
    Nodo *izq;
    Nodo *padre;
};

//Prototipos de función
Nodo *crearNodo(int n,Nodo *);
void menu();
void insertarNodo(Nodo *&,int n,Nodo *);
void mostrarArbol(Nodo *,int);
bool busqueda(Nodo *,int);
void preOrden(Nodo *);
void inOrden(Nodo *);
void postOrden(Nodo *);
void eliminar(Nodo *,int);
void eliminarNodo(Nodo *);
Nodo *minimo(Nodo *);
void reemplazar(Nodo *,Nodo *);
void destruirNodo(Nodo *);

//Inicializamos el arbol
Nodo *arbol=NULL;

int main(){
    menu();
    return 0;
}

//funcion para mostrar menu
void menu(){
    int opcion,dato,cont=0;
    do{
        cout<<"\tMENU\n";
        cout<<"1.Insertar nuevo nodo\n";
        cout<<"2.Mostrar arbol\n";
        cout<<"3.Mostrar un elemento en el arbol\n";
        cout<<"4.Mostrar el arbol en preOrden\n";
        cout<<"5.Mostrar el arbol en inOrden\n";
        cout<<"6.Mostrar el arbol en postOrden\n";
        cout<<"7.Eliminar un nodo\n";
        cout<<"8.Salir\n";
        cout<<"Opcion: "; cin>>opcion;

        switch (opcion)
        {
        case 1:
            cout<<"Ingrese el valor a insertar: "; cin>>dato;
            insertarNodo(arbol,dato,NULL);
            break;
        case 2:
            cout<<"Mostrando arbol completo\n\n";
            mostrarArbol(arbol,cont);
            cout<<endl;
            break;
        case 3:
            cout<<"Elemento que desea buscar "; cin>>dato;
            if (busqueda(arbol,dato)){
                cout<<"El elemento "<<dato<<" fue encontrado en el arbol"<<endl;
            }else{
                cout<<"El elemento "<<dato<<" no fue encontrado en el arbol"<<endl;
            }
            cout<<endl;
            break;
        case 4:
            cout<<"\nRecorrido en preOrden"<<endl;
            preOrden(arbol);
            cout<<"\n";
            break;
        case 5:
            cout<<"\nRecorrido en inOrden"<<endl;
            inOrden(arbol);
            cout<<"\n";
            break;
        case 6:
            cout<<"\nRecorrido en postOrden"<<endl;
            postOrden(arbol);
            cout<<"\n";
            break;
        case 7:
            cout<<"Ingrese el dato a eliminar del nodo: "; cin>>dato;
            eliminar(arbol,dato);
            cout<<"\n";
            break;
        case 8:
            cout<<"Programa terminado"<<endl;
            break;
        default:
            cout<<"Opcion incorrecta"<<endl;
            break;
        }
    } while (opcion!=8);
}//fin de menu

//Funcion que solo crea un nodo
Nodo *crearNodo(int n,Nodo *padre){
    Nodo *nuevo_nodo=new Nodo();
    nuevo_nodo->dato=n;
    nuevo_nodo->der=NULL;
    nuevo_nodo->izq=NULL;
    nuevo_nodo->padre=padre;
    return nuevo_nodo;
}//fin de crearNodo

//Fucnion para insertar un nodo
void insertarNodo(Nodo *&arbol,int n,Nodo *padre){
    if (arbol==NULL){//si el arbol esta vacio
        Nodo *nuevo_nodo=crearNodo(n,padre);
        arbol=nuevo_nodo;
    }else{
        //obtenemos el valor de la raiz
        int valorRaiz=arbol->dato;
        //si n es menor al valor del nodo raiz se inserta a la izquierda
        if (n<valorRaiz){
            insertarNodo(arbol->izq,n,padre);
        }//si n es mayor al valor del nodo raiz se inserta a la derecha
        else{
            insertarNodo(arbol->der,n,padre);
        }
    }
}//fin de insertarNodo

//Funcion para mostrar el arbol binario
void mostrarArbol(Nodo *arbol,int contador){
    if (arbol==NULL){
        return;
    }else{
        mostrarArbol(arbol->der,contador+1);
        for (int i = 0; i < contador; i++){
            cout<<"  ";
        }
        cout<<arbol->dato<<endl;
        mostrarArbol(arbol->izq,contador+1);
    }
}//fin de mostrarArbol

//funcion para buscar un valor en el arbol
bool busqueda(Nodo *arbol,int n){
    //el arbol esta vacio
    if (arbol==NULL){
        return false;
    }else if(arbol->dato==n){     //se encontro el dato en el nodo
        return true;
    }else if(n<arbol->dato){    //el dato es menor al dato del nodo y se tiene que buscar por la izquierda
        return busqueda(arbol->izq,n);
    }else{                      //el dato es mayor al dato del nodo y se busca por la derecha
        return busqueda(arbol->der,n);
    }
}//fin de busqueda

//funcion para mostrar el arbol en preOrden
void preOrden(Nodo *arbol){
    if (arbol==NULL){
        return;
    }else{
        cout<<arbol->dato<<"-";
        preOrden(arbol->izq);
        preOrden(arbol->der);
    }
}//fin de preOrden

//funcion para mostrar el arbol en inOrden
void inOrden(Nodo *arbol){
    if (arbol==NULL){
        return;
    }else{
        inOrden(arbol->izq);
        cout<<arbol->dato<<" - ";
        inOrden(arbol->der);
    }
}//fin de inOrden

//funcion para mostrar el arbol en postOrden
void postOrden(Nodo *arbol){
    if (arbol==NULL){
        return;
    }else{
        postOrden(arbol->izq);
        postOrden(arbol->der);
        cout<<arbol->dato<<" - ";
    }
}//fin de postOrden
//funcion para eliminar un nodo de arbol
void eliminar(Nodo *arbol,int n){
    if(arbol==NULL){ //Nose encontro nada
        return;
    }else if(n<arbol->dato){    //se busca por la izquierda
        eliminar(arbol->izq,n);
    }else if(n>arbol->dato){    //se busca por la derecha
        eliminar(arbol->der,n);
    }else{  //ya se encontro el valor en el arbol
        eliminarNodo(arbol);
    }
}//fin de eliminar

//funcion para eliminar el nodo encontrado
void eliminarNodo(Nodo *nodoEliminar){
    if (nodoEliminar->izq && nodoEliminar->der){ //para saber si el nodo tiene hijo izquierdo y derecho
        Nodo *menor=minimo(arbol->der);
        nodoEliminar->dato=menor->dato;
        eliminarNodo(menor);
    }
    else if(nodoEliminar->izq){         //si tiene solo hijo izquierdo
        reemplazar(nodoEliminar,nodoEliminar->izq);
        destruirNodo(nodoEliminar);
    }else if(nodoEliminar->der){         //si tiene solo hijo derecho
        reemplazar(nodoEliminar,nodoEliminar->der);
        destruirNodo(nodoEliminar);
    }else{                              //nodo sin hijos
        reemplazar(nodoEliminar,NULL);
        destruirNodo(nodoEliminar);
    }
    
}//fin de eliminarNodo

//funcion para determinar el nodo más izquierdo posible
Nodo *minimo(Nodo *arbol){
    if (arbol==NULL){
        return NULL; //si el arbol esta vacio retornas NULL
    }
    if(arbol->izq){ //si tiene hijo izquierdo
        return  minimo(arbol->izq);     //buscamos el hijo mas a la izquierda
    }else{          //si no tenemos hijo izquierdo
        return arbol;   //regresamos el mismo nodo
    }   
}//fin de minimo

//funcion para reemplazar nodo para nodos con un solo hijo
void reemplazar(Nodo *arbol,Nodo *nuevoNodo){
    if (arbol->padre){
        //arbol ->padre hay que asignarle el nuevo hijo
        if (arbol->dato==arbol->padre->izq->dato){      //para cambiar el hijo izquierdo
            arbol->padre->izq=nuevoNodo;
        }else if(arbol->dato==arbol->padre->der->dato){   //para cambiar el hijo derecho
            arbol->padre->der=nuevoNodo;
        }  
    }
    if (nuevoNodo){
        //procedemos asiganarle el nuevo padre
        nuevoNodo->padre=arbol->padre;
    }   
}

//funcion para destruir el nodo
void destruirNodo(Nodo *destruir){
    destruir->izq=NULL;
    destruir->der=NULL;

    delete destruir;    
}