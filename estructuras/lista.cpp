#include <iostream>
#include <stdlib.h>

using namespace std;

struct Nodo
{
    int dato;
    Nodo *siguiente;
};

void menu();
void insertarLista(Nodo *&,int);
void mostrarLista(Nodo *);
bool buscarLista(Nodo *,int);
void eliminarLista(Nodo *&,int);
void eliminarTodaLista(Nodo *&,int &);
void calcularMayor(Nodo *&,int &);
void calcularMenor(Nodo *&,int &);

Nodo *lista=NULL;

int main(){
    menu();
    return 0;
}

void menu(){
    int opcion,dato;
    bool bandera;

    do
    {
        cout<<"\t MENU \n";
        cout<<"1.Insertar elementos a la lista\n";
        cout<<"2.Mostrar elementos de la lista\n";
        cout<<"3.Buscar elemento\n";
        cout<<"4.Eliminar elemento\n";
        cout<<"5.Eliminar toda la lista\n";
        cout<<"6.Mostrar el mayor de la lista\n";
        cout<<"7.Mostrar el menor de la lista\n";
        cout<<"8.Salir\n";
        cout<<"Opcion: ";cin>>opcion;

        switch (opcion)
        {
        case 1:
            cout<<"\nIngrese el dato: "; cin>>dato;
            insertarLista(lista,dato);
            cout<<"\n";
            break;
        
        case 2:
            mostrarLista(lista);
            break;
        case 3:
            cout<<"\nIngrese dato a buscar: ";cin>>dato;
            bandera=buscarLista(lista,dato);
            if(bandera){
                cout<<"El dato "<<dato<<" fue encontrado"<<endl;
            }else{
                cout<<"El dato NO "<<dato<<" fue encontrado"<<endl;
            }
            break;
        case 4:
            cout<<"\nIngrese el dato a eliminar: ";cin>>dato;
            eliminarLista(lista,dato);
            break;
        case 5:
            while (lista!=NULL){
                eliminarTodaLista(lista,dato);
                cout<<"Dato eliminado "<<dato<<endl;
            }
            break;
        case 6:
            calcularMayor(lista,dato);
            cout<<"El número mayor de la lista es "<<dato<<endl;
            break;
        case 7:
            calcularMenor(lista,dato);
            cout<<"El número menor de la lista es "<<dato<<endl;
            break;
        default:
            break;
        }
        //system("clear");
    } while (opcion>0 && opcion<8);
    
}

void insertarLista(Nodo *&lista,int n){
    Nodo *nuevo_nodo=new Nodo();
    nuevo_nodo->dato=n;
    //se le asigna aux1 al ultimo elemento de la lista insertado
    Nodo *aux1=lista;
    Nodo *aux2;
    //si aux es diferente de NULL hay elementos en la lista
    //y si el dato a insertar es mayor al que apunta la lista
    //el nodo nuevo se va a insertar a la derecha para ordenarlo
    while ((aux1!=NULL) && (aux1->dato<n)){
        //aux2 apuntara al elemento lista
        aux2=aux1;
        //aux1 apuntara al ultimo elemento
        aux1=aux1->siguiente;
    }

    if(lista==aux1){
        //si el elemento a insertar es menor,se inserta a la izquierda(menor)
        lista=nuevo_nodo;
    }else{
        //si el elemento a insertar es mayor,se inserta a la derecha(mayor)
        aux2->siguiente=nuevo_nodo;
    }

    nuevo_nodo->siguiente=aux1;
    cout<<"\tElemento "<<n<<" insertado en la lista\n";
}
//funcion para recorrer y mostrar el dato de cada nodo
void mostrarLista(Nodo *lista){
    Nodo *actual=new Nodo();
    actual=lista;
    
    while (actual!=NULL){
        cout<<actual->dato<<"->";
        actual=actual->siguiente;
    }
}
//funcion para encontrar un elemento a la lista
bool buscarLista(Nodo *lista,int n){
    bool encontrado=false;
    Nodo *actual=new Nodo();
    actual=lista;

    while ((actual!=NULL) && (actual->dato<=n)){
        if(actual->dato==n){
            encontrado=true;
        }
        //avanza al siguiente nodo
        actual=actual->siguiente;
    }
    return encontrado;
}

//funcion para eliminar un elemento de la lista
void eliminarLista(Nodo *&lista,int n){
    if (lista!=NULL){
        Nodo *aux_borrar=new Nodo();
        Nodo *anterior=NULL;
        aux_borrar=lista;
        //mientras no sea el final de la lista y el dato del nodo
        //no sea igual al dato que se le paso para borrar
        //se van actualizado los apuntadore
        while ((aux_borrar!=NULL) && (aux_borrar->dato!=n)){
            anterior=aux_borrar;
            aux_borrar=aux_borrar->siguiente;
        }//fin de while
        //llego al final de la lista y no encontro nada
        if (aux_borrar==NULL){
            cout<<"Elemento "<<n<<"no encontrado."<<endl;
        }//el elemento a borrar es el primero de la lista
        else if (anterior==NULL){
            lista=lista->siguiente;
            delete aux_borrar;
            cout<<"Se elimino el elemento "<<n<<endl;
        }//el elemento esta en la lista pero no esta en el primer nodo
        else{
            anterior->siguiente=aux_borrar->siguiente;
            delete aux_borrar;
            cout<<"Se elimino el elemento "<<n<<endl;
        }      
    }//fin de if
}//fin de eliminarLista
//funcion para eliminar todos los elementos de la lista
void eliminarTodaLista(Nodo *&lista,int &n){
    Nodo *aux_borrar=lista; 
    n=aux_borrar->dato;
    lista=aux_borrar->siguiente;
    delete aux_borrar;
}//eliminarTodaLista
//funcion para calcular el mayo numero de una lista
void calcularMayor(Nodo *&lista,int &n){
    int mayor=0;
    Nodo *aux_nodo=lista;

    while (aux_nodo!=NULL){
        if (aux_nodo->dato>mayor){
            mayor=aux_nodo->dato;
        }
        aux_nodo=aux_nodo->siguiente;
    }
    n=mayor;
}//fin de calcularMayor
void calcularMenor(Nodo *&lista,int &n){
    n=lista->dato;
}