#include <iostream>
#include <stdlib.h>

using namespace std;

struct Nodo
{
    int dato;
    Nodo *siguiente;
};

//Prototipos de funcion
bool cola_vacia(Nodo *);
void insertarNodo(Nodo *&,Nodo *&,int);
void eliminarNodo(Nodo *&,Nodo *&,int &);

int main(){
    Nodo *frente=NULL;
    Nodo *fin=NULL;
    int numNodos,dato;

    cout<<"Ingrese el numero de nodos:  "; cin>>numNodos;

    for(int i=0;i<numNodos;i++){
        cout<<"Ingrese el dato para el nodo["<<i<<"]: "; cin>>dato;
        insertarNodo(frente,fin,dato);
    }

    cout<<"\nMostrando cola"<<endl;
    while(frente!=NULL && fin!=NULL){
        eliminarNodo(frente,fin,dato);
        cout<<dato<<" ";
    }
    cout<<endl; 
    return 0;
}

void insertarNodo(Nodo *&frente,Nodo *&fin,int n){
    Nodo *nuevo_nodo=new Nodo();
    nuevo_nodo->dato=n;
    //los nuevos nodos siempre van apuntar a null hasta que llegue uno nuevo
    nuevo_nodo->siguiente=NULL;

    //si devuelve false quiere decir que es el primer elemento de la cola
    if(cola_vacia(frente)){
        //como es el primer elemento se le asigna nuevo_nodo a frente
        frente=nuevo_nodo;
    }else{
        //al ultimo elemento de la cola se le reedirige al nuevo_nodo para que deje de apuntar a NULL
        fin->siguiente=nuevo_nodo;
    }
    //se asigna fin al ultimo elemento agregado
    fin=nuevo_nodo;
}
//funcion para eliminar nodos de la cola
void eliminarNodo(Nodo *&frente,Nodo *&fin,int &n){
    n=frente->dato;
    Nodo *aux=frente;

    if(frente==fin){
        frente=NULL;
        fin=NULL;
    }else{
        frente=frente->siguiente;
    }
    delete aux;

}

//Si el apuntador señala a NULL no hay elemntos en la cola
bool cola_vacia(Nodo *frente){
    return (frente==NULL)?true:false;
}