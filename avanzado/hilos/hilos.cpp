#include<iostream>
#include<thread>

using namespace std;

void func(int,double);
void sum(int,int);

int main(){
    thread th(func,5,8.91);
    thread th2(sum,10,8);
    thread th3(sum,9,1);

    th.join();
    th2.join();
    th3.join();
    /*
    if (th.joinable() && th2.joinable() && th3.joinable()){
        th.join();
        th2.join();
        th3.join();    
    }*/
    return 0;
}

void func(int n,double m){
    cout<<n<<" "<<m<<endl;
}

void sum(int n,int m){
    cout<<"La suma es: "<<(n+m)<<endl;
}
/*
    Compilar  g++ -std=c++11 hilos.cpp -lpthread -o hilos
*/