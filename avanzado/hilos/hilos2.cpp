//Ejemplo de hilos con mutex para evitar problemas con la concurrencia
#include <iostream>
#include <stdlib.h>
#include <mutex>
#include <thread>
#include <chrono>
#include <string>

using namespace std;

//Variable compartida
static int numero=0;

//Prorototipo de sumar
void suma(int);

int main(){
    //Lanzamos varios hilos,unos suman y otros restan
    thread([](){for(int i=0;i<30;++i)suma(-i);}).detach();
    thread([](){for(int i=0;i<16;++i)suma(i);}).detach();
    thread([](){for(int i=0;i<20;++i)suma(-i);}).detach();
    thread([](){for(int i=0;i<16;++i)suma(i);}).detach();
    thread([](){for(int i=0;i<30;++i)suma(i);}).detach();
    thread([](){for(int i=0;i<16;++i)suma(i);}).detach();
    thread([](){for(int i=0;i<20;++i)suma(i);}).detach();
    thread([](){for(int i=0;i<16;++i)suma(i);}).detach();

    // Introducimos una espera para que todos los threads hayan terminado
    this_thread::sleep_for(std::chrono::milliseconds(1000));
    cout<<endl<<numero<<endl;
    return 0;
}

void suma(int n){
    static mutex m;
    this_thread::sleep_for(chrono::milliseconds(n));
    //Bloquea la variable numero para evitar problemas de concurrencia
    m.lock();
    numero+=n;
    //Desbloquea la variable
    m.unlock();
}