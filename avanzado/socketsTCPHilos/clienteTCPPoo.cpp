/*Cliente TCP con programación orientada a objetos*/
#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <string>
#include "SocketTCP.h"

using namespace std;

int main(int argc, char*argv[]){
    //paso de parametros ip y puerto ./cliente 127.0.0.1 54000
    char* ip = (char *) argv[1];
    string ipAddress(ip);
    int puerto=atoi(argv[2]);
    //constructor para el cliente
    SocketTCP tcpCliente=SocketTCP(puerto,ipAddress);
    
    string userInput;
    do{
        //Cadena a enviar al servidor
        cout << "> ";
        getline(cin, userInput);

        //Envia al servidor
        int sendRes = tcpCliente.envia(userInput);
        if (sendRes == -1){
            cout << "No se ha podido enviar al servidor\r\n";
            continue;
        }
        int bytesReceived = tcpCliente.recibe();
        if (bytesReceived == -1)
        {
            cout << "Ha ocurrido un error con la respuesta del servidor\r\n";
        }

    } while (true);

    //cierra el socket
    tcpCliente.~SocketTCP();
    return 0;
}