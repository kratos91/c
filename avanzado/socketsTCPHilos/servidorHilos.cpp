#include <iostream>
#include <string.h>
#include <string>
#include "SocketTCP.h"
#include <thread>

using namespace std;

void correrHilo(int,int);

int main(int argc, char*argv[]){
    int puerto1,puerto2;
    cout<<"Ingrese el puerto 1: ";  cin>>puerto1;
    cout<<"Ingrese el puerto 2: ";  cin>>puerto2;

    thread servidor1(correrHilo,puerto1,1);
    thread servidor2(correrHilo,puerto2,2);

    servidor1.join();
    servidor2.join();
    
    return 0;
}

void correrHilo(int puerto,int hilo){
    //Constructor para el servidor
    SocketTCP tcpServidor=SocketTCP(puerto);
    // While que espera al cliente
    char buf[4096];
 
    while (true)
    {
        cout<<"Hilo "<<hilo<<" puerto "<<puerto<<endl;
        // Esperando a recibir datos
        int bytesReceived = tcpServidor.recibe();
        if (bytesReceived == -1){
            cerr << "Error en recv()" << endl;
            break;
        }
 
        if (bytesReceived == 0){
            cout << "Cliente desconectado " << endl;
            break;
        }

        //Cadena a enviar al cliente
        string strtemp;
        cout<<" > ";
        getline(cin, strtemp);
        
        tcpServidor.envia(strtemp);

    }
 
    // Cierra el socket socket
    tcpServidor.~SocketTCP();
}