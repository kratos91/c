#ifndef __SOCKETTCP_H__
#define __SOCKETTCP_H__

using namespace std;

class SocketTCP
{
private:
    int _s;
    int sock,bytesReceived,listening,connectRes;
    char buf[4096];
    string ipAddress;
public:
    SocketTCP(int);
    SocketTCP(int,string);
    char * getBuffer();
    int recibe();
    int envia(string);
    ~SocketTCP();
};

#endif