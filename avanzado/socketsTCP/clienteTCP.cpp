/*Cliente TCP sin programación orientada a objetos*/
#include <iostream>
#include <sys/types.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <string.h>
#include <string>

using namespace std;

int main(int argc, char*argv[])
{
    //se le pasa como argumento el puerto y la IP en el que va a escuchar el servidor
    char* ip = (char *) argv[1];
    int puerto=atoi(argv[2]);
    //	Crea un socket
    int sock = socket(AF_INET, SOCK_STREAM, 0);
    if (sock == -1)
    {
        cout<<"No se pudo conectar el cliente"<<endl;
        return 1;
    }

    //string ipAddress = "127.0.0.1";

    sockaddr_in hint;
    hint.sin_family = AF_INET;
    hint.sin_port = htons(puerto);
    //inet_pton(AF_INET, ipAddress.c_str(), &hint.sin_addr);
    inet_pton(AF_INET, ip, &hint.sin_addr);

    //	Conecta con el servidor
    int connectRes = connect(sock, (sockaddr*)&hint, sizeof(hint));
    
    if (connectRes == -1)
    {
        return 1;
    }

    //	While que se encarga de escuchar las respuestas del servidor
    char buf[4096];
    string userInput;


    do {
        //Cadena a enviar al servidor
        cout << "> ";
        getline(cin, userInput);

        //Envia al servidor
        int sendRes = send(sock, userInput.c_str(), userInput.size() + 1, 0);
        if (sendRes == -1)
        {
            cout << "No se ha podido enviar al servidor\r\n";
            continue;
        }

        //espera la respuesta
        memset(buf, 0, 4096);
        int bytesReceived = recv(sock, buf, 4096, 0);
        if (bytesReceived == -1)
        {
            cout << "Ha ocurrido un error con la respuesta del servidor\r\n";
        }
        else
        {
            //Muestra la cadena enviada desde el servidor
            cout << "SERVER> " << string(buf, bytesReceived) << "\r\n";
        }
    } while(true);

    //Cierra el socket
    close(sock);

    return 0;
}