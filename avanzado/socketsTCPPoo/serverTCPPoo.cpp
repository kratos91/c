#include <iostream>
#include <string.h>
#include <string>
#include "SocketTCP.h"
 
using namespace std;

int main(int argc, char*argv[])
{   
    //se le pasa como argumento el puerto en el que va a escuchar el servidor e.j ./servidor 54000
    int puerto=atoi(argv[1]);
    //Constructor para el servidor
    SocketTCP tcpServidor=SocketTCP(puerto);
    // While que espera al cliente
    char buf[4096];
 
    while (true)
    {
        // Esperando a recibir datos
        int bytesReceived = tcpServidor.recibe();
        if (bytesReceived == -1){
            cerr << "Error en recv()" << endl;
            break;
        }
 
        if (bytesReceived == 0){
            cout << "Cliente desconectado " << endl;
            break;
        }

        //Cadena a enviar al cliente
        string strtemp;
        cout<<" > ";
        getline(cin, strtemp);
        
        tcpServidor.envia(strtemp);

    }
 
    // Cierra el socket socket
    tcpServidor.~SocketTCP();
 
    return 0;
}