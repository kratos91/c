#include <iostream>
#include <sys/types.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <string.h>
#include <string>
#include "SocketTCP.h"

using namespace std;

SocketTCP::SocketTCP(int puerto)
{
   // Crea el socket si es 0 o mayor no hay problemas
    listening = socket(AF_INET, SOCK_STREAM, 0);
    if (listening == -1)
    {
        cerr << "No se pudo crear el socket" << endl;
    }
 
    // Enlaza la ip y el puerto en el que va a escuchar el servidor
    sockaddr_in hint;
    hint.sin_family = AF_INET;
    hint.sin_port = htons(puerto);
    inet_pton(AF_INET, "0.0.0.0", &hint.sin_addr);
 
    bind(listening, (sockaddr*)&hint, sizeof(hint));
 
    // Le dice Winsock el numero de escuchas 128 por defecto de linux
    listen(listening, SOMAXCONN);
 
    // Espera a que el un cliente se conecte
    sockaddr_in client;
    socklen_t clientSize = sizeof(client);
 
    sock = accept(listening, (sockaddr*)&client, &clientSize);
 
    char host[NI_MAXHOST];      // Nombre del cliente
    char service[NI_MAXSERV];   // Puerto en el que se conecta el cliente al servidor
 
    memset(host, 0, NI_MAXHOST); 
    memset(service, 0, NI_MAXSERV);
 
    if (getnameinfo((sockaddr*)&client, sizeof(client), host, NI_MAXHOST, service, NI_MAXSERV, 0) == 0)
    {
        cout << host << " conectado en el puerto " << service << endl;
    }
    else
    {
        inet_ntop(AF_INET, &client.sin_addr, host, NI_MAXHOST);
        cout << host << " conectado en el puerto " << ntohs(client.sin_port) << endl;
    }
 
    // Cierra el socket que abre la comunicación con el cliente
    close(listening);
 
}

SocketTCP::SocketTCP(int puerto,string ip){
    //	Crea un socket
    sock = socket(AF_INET, SOCK_STREAM, 0);
    if (listening == -1)
    {
        cout<<"No se pudo crear el socket"<<endl;
    }


    sockaddr_in hint;
    hint.sin_family = AF_INET;
    hint.sin_port = htons(puerto);
    inet_pton(AF_INET, ip.c_str(), &hint.sin_addr);

    //	Conecta con el servidor
    connectRes = connect(sock, (sockaddr*)&hint, sizeof(hint));
    
    if (connectRes == -1)
    {
        cout<<"Error al conectar con el servidor"<<endl;
    }

}

//función que regresa la cadena que le llega al servidor
char * SocketTCP::getBuffer(){
    return buf;
}

int SocketTCP::recibe(){
    memset(buf, 0, 4096);
    // Espera a que se le envie algo
    bytesReceived = recv(sock, buf, 4096, 0);
    cout<<"CLIENT> "<<buf<<endl;
    //si la respuesta es mayor a 0 salio bien todo
    return bytesReceived;
}

int SocketTCP::envia(string cadena){
    //envia la cadena al cliente que se le paso desde el main
    _s=send(sock, cadena.c_str(), cadena.size()+1, 0);
    return _s;
}

SocketTCP::~SocketTCP()
{
    close(sock);
}
