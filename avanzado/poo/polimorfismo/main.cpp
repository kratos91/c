#include <iostream>
#include "Rectangulo.h"
#include "Triangulo.h"
#include "Poligono.h"

using namespace std;

int main(){
    Poligono *poligonos[2];

    poligonos[0]=new Rectangulo(5.5,7.9);
    poligonos[1]=new Triangulo(3.4,2.1,4.0);

    for (int i = 0; i < 2; i++){
        cout<<"Area: "<<poligonos[i]->area()<<endl;
        cout<<"Perimetro: "<<poligonos[i]->perimetro()<<endl;
    }
    
    return 0;
}