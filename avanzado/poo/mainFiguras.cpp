#include <iostream>
#include <stdlib.h>
#include "Triangulo.h"

using namespace std;

int main(){
    Triangulo *t1=new Triangulo(3,5,6,7);

    cout<<"Numero de lados de la figura: "<<t1->getLados()<<endl;
    cout<<"Area del triangulo: "<<t1->areaTriangulo()<<endl;

    return 0;
}