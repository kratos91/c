#include <iostream>

using namespace std;

class Avion
{
private:
    string modelo;
public:
    Avion(string modelo){
        this->modelo=modelo;
    }
    ~Avion(){}
    void indicarAvion(){
        cout<<"Avion se desplaza por aire"<<endl;
    }

    string getModelo(){
        return modelo;
    }
};

