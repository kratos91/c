#include <iostream>
#include "Barco.h"
#include "Avion.h"

using namespace std;

class HidroAvion : public Barco,public Avion
{
private:
    string codigo;
public:
    HidroAvion(string nombre,string modelo,string codigo):Barco(nombre),Avion(modelo){
        this->codigo=codigo;
    }
    ~HidroAvion(){}

    void mostrarDatos(){
        cout<<"Nombre: "<<getNombreBarco()<<endl;
        cout<<"Modelo: "<<getModelo()<<endl;
        cout<<"Codigo: "<<this->codigo<<endl; 
    }

    string getCodigo(){
        return codigo;
    }
};
