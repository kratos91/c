//Funccion principal que implmenta clases abstractas con metodos virtuales
#include <iostream>
#include <stdlib.h>
#include "Planta.h"
#include "Carnivoro.h"
#include "Hervivoro.h"

using namespace std;

int main(){
    Planta *planta=new Planta();
    Carnivoro *car=new Carnivoro();
    Hervivoro *her=new Hervivoro();

    planta->alimentarse();
    car->alimentarse();
    her->alimentarse();

    return 0;
}