#include <iostream>
#include <sys/wait.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>

using namespace std;

int main(){
    pid_t pid1,pid2;
    int status1,status2;

    if ((pid1=fork())==0)
    {/* Primer hijo */
        cout<<"Soy el primer hijo ("<<getpid()<<" hijo de "<<getppid()<<")"<<endl;
    }else
    {/*     Padre   */
        if ((pid2=fork())==0)
        {/* Segundo hijo  */
            cout<<"Soy el segundo hijo ("<<getpid()<<" hijo de "<<getppid()<<")"<<endl;
        }else
        {/* Entro al proceso padre*/
            /*Esperamos al primer hijo*/
            waitpid(pid1,&status1,0);
            /*Esperamos al segundo hijo*/
            waitpid(pid2,&status2,0);

            cout<<"Soy el padre ("<<getpid()<<" hijo de "<<getppid()<<")"<<endl;
        }
        
    }
    
    return 0;
}