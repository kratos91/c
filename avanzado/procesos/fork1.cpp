//Programa de ejemplo para crear un proceso hijo y esperar a que termine 
#include <iostream>
#include <sys/wait.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>

using namespace std;

int main(){
    int pid,i,estado;

    pid=fork();

    switch (pid){
    case -1:    //Si pid es -1 quiere decir que hubo un error a crear el proceso hijo
        cout<<"Ha ocurrido un error al crear el proceso hijo"<<endl;        
        break;
    case 0:     //Cuando pid==0 quiere decir que se esta ejecutando el hijo
        cout<<"Soy el proceso hijo ejecutandose..."<<endl;
        for (int i = 0; i < 5; i++){
            cout<<"Soy el proceso hijo..."<<endl;
        }
        
        break;
    default:
        //wait forza al padre a esperar a que el proceso hijo termine para el empezar
        wait(&estado);
        cout<<"Mi proceso hijo ya ha terminado"<<endl;
        for (int i = 0; i < 5; i++){
            cout<<"Soy el proceso padre..."<<endl;
        }
        break;
    }
    return 0;
}