#include <iostream>
#include "intercambio.h"

using namespace std;

template <typename T>
void ordenarAsc(T *arreglo,int n){
    for (int i = 0; i < n; i++){
        for (int j = 0; j < (n-1); j++){
            if(arreglo[j]>arreglo[j+1]){
                intercambio(arreglo[j],arreglo[j+1]);
            }
        }
    }
}//fin de ordenarAsc


template <typename T>
void ordenarDesc(T *arreglo,int n){
    for (int i = 0; i < n; i++){
        for (int j = 0; j < (n-1); j++){
            if(arreglo[j]<arreglo[j+1]){
                intercambio(arreglo[j],arreglo[j+1]);
            }
        }
    }
}//fin de ordenarDesc