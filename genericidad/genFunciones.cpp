#include <iostream>
#include "Mayor.h"

using namespace std;

int main(){
    cout<<"El mayor de dos numeros enteros es: "<<mayor(5,8)<<endl;
    cout<<"El mayor de dos numeros flotantes es: "<<mayor(5.5,5.4)<<endl;
    cout<<"El mayor de dos caracteres es: "<<mayor('r','z')<<endl;
    return 0;
}