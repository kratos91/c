#include <iostream>
#include <stdlib.h>
#include "Arreglo.h"

using namespace std;

void opciones(){
    cout<<"\t MENU \n"<<endl;
    cout<<"1.Agregar elemento al arreglo"<<endl;
    cout<<"2.Comprobar el espacio en el arreglo"<<endl;
    cout<<"3.Ver arreglo"<<endl;
    cout<<"4.Vaciar arreglo"<<endl;
    cout<<"5.Salir"<<endl;
}
int main(){
    int opcion,num;
    Arreglo <int> arreglo1(5);

    do
    {
        opciones();
        cout<<"Eliga una opcion:"; cin>>opcion;
        switch (opcion)
        {
        case 1:
            cout<<"Elemento a agregar: "; cin>>num;
            if(arreglo1.arregloLleno()){
                cout<<"\nArreglo lleno..."<<endl;
            }else{
                arreglo1.agregar(num);
            }
            break;
        
        case 2:
            if (arreglo1.arregloLleno()){
                cout<<"Arreglo lleno"<<endl;
            }else{
                cout<<"Arreglo con espacio"<<endl;
            }
            
            break;
        case 3:
            arreglo1.mostrarArreglo();
            break;
        case 4:
            cout<<"\nVaciando arreglo"<<endl;
            arreglo1.vaciarArreglo();
            break;
        case 5:
            cout<<"Programa terminado"<<endl;
            break;
        default:
            cout<<"Opcion incorrecta del menu"<<endl;
            break;
        }
    } while (opcion!=5);
    
    return 0;
}