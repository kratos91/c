#include <iostream>
#include "intercambio.h"

using namespace std;

int main(){
    float dato1,dato2;

    cout<<"Digite el valor de dato1: "; cin>>dato1;
    cout<<"Digite el valor de dato2: "; cin>>dato2;

    //intercambiar el valor de las dos variables
    intercambio(dato1,dato2);

    cout<<"El valor de dato1 despues del intercambio: "<<dato1<<endl;
    cout<<"El valor de dato2 despues del intercambio: "<<dato2<<endl;

    return 0;
}