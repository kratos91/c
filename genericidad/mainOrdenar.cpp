#include <iostream>
#include "ordenar.h"

using namespace std;

int main(){
    int numEle;

    cout<<"Digite la cantidad de elementos del arreglo: "; cin>>numEle;
    int *elementos=new int[numEle];

    for (int i = 0; i < numEle; i++){
        cout<<"Ingrese el elemento["<<i<<"] del arreglo: "; cin>>elementos[i];
    }

    cout<<"Ordenado ascendetemente."<<endl;

    ordenarAsc(elementos,numEle);
    for (int i = 0; i < numEle; i++){
        cout<<"El elemento["<<i<<"] del arreglo es "<<elementos[i]<<endl;
    }
    
    cout<<"Ordenado descendetemente."<<endl;

    ordenarDesc(elementos,numEle);
    for (int i = 0; i < numEle; i++){
        cout<<"El elemento["<<i<<"] del arreglo es "<<elementos[i]<<endl;
    }
    
    delete[] elementos;
    return 0;
} 
