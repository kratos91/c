#include<iostream>
#include<stdlib.h>
#include<time.h>

using namespace std;


int main(){
    int dato,nfilas,ncolumnas;

    cout<<"Digite el numero de filas: ";
    cin>>nfilas;

    cout<<"Digite el numero de columnas: ";
    cin>>ncolumnas;

    int numeros[nfilas][ncolumnas],copia[nfilas][ncolumnas];

    srand(time(NULL));
    
    //Rellenar la matriz
    for(int i=0;i<nfilas;i++){
        for(int j=0;j<ncolumnas;j++){
            dato=1+rand()%(100);
            numeros[i][j]=dato;        
        }
    }

    //Mostrar la matriz
    for(int i=0;i<nfilas;i++){
        for(int j=0;j<ncolumnas;j++){
            cout<<numeros[i][j]<<" ";        
        }
        cout<<"\n";
    }
    

    return 0;
}