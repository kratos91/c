//Ordenamiento por burbuja
#include<iostream>
#include<stdlib.h>

using namespace std;

int main(){
    int numeros[]={4,1,5,2,3};
    int i,j,aux=0;

    //Algoritmo del metodo burbuja
    for(i=0;i<5;i++){
        for(j=0;j<5;j++){
            if(numeros[j]>numeros[j+1]){
                aux=numeros[j];
                numeros[j]=numeros[j+1];
                numeros[j+1]=aux;
            }
        }
    }

    for(i=0;i<5;i++){
        cout<<numeros[i]<<endl;
    }
    return 0;
}