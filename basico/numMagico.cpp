#include<iostream>
#include<stdlib.h>
#include<time.h>

using namespace std;

int main(){
    int intentos=0,dato,numero;

    //genera numero aleatorio
    srand(time(NULL));
    dato=1 + rand()%(100+1-1);

    do{
        cout<<"Digite un numero "; cin>>numero;

        if(numero>dato){
            cout<<"\n Digite un numero menor\n";
        }
        if(numero<dato){
            cout<<"\n Digite un numero mayor\n";
        }

        intentos++;
    }while(numero!=dato);

        cout<<"El numero de intentos es: "<<intentos <<" para adivnir el numero: "<<dato;
    return 0;
}