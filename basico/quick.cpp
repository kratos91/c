//Ordenamiento por quick sort
#include<iostream>
#include<stdlib.h>

using namespace std;

void intercambio(float &x,float &y){
    float aux;

    aux=x;
    x=y;
    y=aux;
}

void quickSort(float a[],int primero,int ultimo){
    int i,j,central;
    float pivote;

    central=(primero+ultimo)/2;
    pivote=a[central];

    i=primero;
    j=ultimo;

    do{
        while(a[i]<pivote) i++;
        while(a[j]>pivote) j--;

        if(i<=j){
            intercambio(a[i],a[j]);
            i++;
            j--;
        }
    }while(i<=j);

    if(primero<j){
        quickSort(a,primero,j);//ordena la sublista izquierda
    }
    if(i<ultimo){
        quickSort(a,i,ultimo); //ordena la sublista derecha
    }
}

int main(){

    float arreglo[]={56,2,43,13,0,65,4,1,54};

    quickSort(arreglo,0,8);

    for(int i=0;i<9;i++){
        cout<<arreglo[i]<<endl;
    }
    return 0;
}