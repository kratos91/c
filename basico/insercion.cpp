//Ordenamiento por insercion
#include<iostream>
#include<stdlib.h>

using namespace std;

int main(){
    int numeros[]={4,1,5,2,3};
    int i,pos,aux;

    //Algoritmo por ordenamiento por insercion
    for(i=0;i<5;i++){
        pos=i;
        aux=numeros[i];

        while((pos>0) && (numeros[pos-1]>aux)){
            numeros[pos]=numeros[pos-1];
            pos--;
        }

        numeros[pos]=aux;
    }

    for(i=0;i<5;i++){
        cout<<numeros[i]<<endl;
    }
    return 0;
}