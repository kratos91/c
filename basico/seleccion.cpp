//Ordenamiento por seleccion
#include<iostream>
#include<stdlib.h>

using namespace std;

int main(){
    int numeros[]={4,1,5,2,3};
    int i,j,aux,min;

    //Algoritmo de insercion
    for(i=0;i<5;i++){
        min=i;
        for(j=i+1;j<5;j++){
            if(numeros[j]<numeros[min]){
                min=j;
            }
        }

        aux=numeros[i];
        numeros[i]=numeros[min];
        numeros[min]=aux;
    }

    for(i=0;i<5;i++){
        cout<<numeros[i]<<endl;
    }

    return 0;
}