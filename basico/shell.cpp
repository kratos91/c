//Ordenamiento por shell
#include<iostream>
#include<stdlib.h>

using namespace std;

void intercambio(float &x,float &y){
    float aux;

    aux=x;
    x=y;
    y=aux;
}

void ordenacionShell(float a[],int n){
    int salto,i,j,k;

    salto=n/2;

    while(salto>0){
        for(i=salto;i<n;i++){
            j=i-salto;
            while(j>=0){
                k=j+salto;
                if(a[j]<=a[k]){ //par de elementos estan ordenados
                    j=-1;
                }else{
                    intercambio(a[j],a[k]);
                    j=-salto;
                }
            }
        }
        salto=salto/2;
    }
}

int main(){
    float arreglo[]={23,1,43,12,6,54,5,0,78};

    ordenacionShell(arreglo,9);

    for(int x=0;x<9;x++){
        cout<<arreglo[x]<<endl;
    }
    return 0;
}
