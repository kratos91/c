//Asignacion dinamica de arreglos
#include <iostream>
#include <stdlib.h>

using namespace std;

void pedirNotas();
void mostarNotas();

int numCalificaciones,*calif;

int main(){
    pedirNotas();
    mostarNotas();

    delete[] calif;
    return 0;
}

void pedirNotas(){
    cout<<"Digite el numero de calificaciones: ";
    cin>>numCalificaciones;

    calif= new int[numCalificaciones];

    for(int i=0;i<numCalificaciones;i++){
        cout<<"Ingrese una nota: ";
        cin>>calif[i];
    }
}

void mostarNotas(){
    cout<<endl;
    cout<<"Mostrar notas"<<endl;
    for(int i=0;i<numCalificaciones;i++){
        cout<<calif[i]<<endl;
    }
}