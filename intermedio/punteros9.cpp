/*
    Crea un arreglo dinamico con new,lo rellena con punteros, despues lo ordena con burbuja
    y lo muestra con punteros
*/
#include <iostream>
#include <stdlib.h>

using namespace std;

int nElementos,*ptrEle;
void pedirDatos();
void ordenarArreglo(int *,int);
void mostrarArreglo(int *,int);

int main(){
    pedirDatos();
    ordenarArreglo(ptrEle,nElementos);
    mostrarArreglo(ptrEle,nElementos);
    //liberar el espacio de memoria del arreglo
    delete[] ptrEle;
    return 0;   
}

void pedirDatos(){
    cout<<"Digite el numero de elementos del arreglo: ";
    cin>>nElementos;
    ptrEle=new int[nElementos];

    for(int i=0;i<nElementos;i++){
        cout<<"Digita un numero["<<i<<"]: ";
        cin>>*(ptrEle+i);
    }
}

void ordenarArreglo(int *ptr,int nEle){
    int aux;
    cout<<"Ordenanando arreglo con burbuja!"<<endl;
    for(int i=0;i<nEle;i++){
        for(int j=0;j<nEle-1;j++){
            if(*(ptr+j)>*(ptr+j+1)){
                aux=*(ptr+j);
                *(ptr+j)=*(ptr+j+1);
                *(ptr+j+1)=aux;
            }
        }
    }
}

void mostrarArreglo(int *ptr,int nEle){
    cout<<"Mostrando arreglo..."<<endl;
    for(int i=0;i<nEle;i++){
        cout<<"Elemento["<<i<<"]="<<*(ptr+i)<<endl;
    }
    cout<<endl;
}