#ifndef VENDEDOR_H
#define VENDEDOR_H

class Vendedor
{
private:
    double totalVentasAnuales();
    double ventas[12];
public:
    Vendedor();
    void obtieneVentasDelUsuario();
    void estableceVentas(int,double);
    void imprimeVentasAnuales();
};


#endif