//Intercambiar el valor de dos variables con punteros
#include <iostream>
#include <stdlib.h>

using namespace std;

void intercambio(float *,float *);
int main(){
    float num1=54.6,num2=23.2;
    cout<<"Primer numero: "<<num1<<endl;
    cout<<"Segundo numero: "<<num2<<endl;

    intercambio(&num1,&num2);
    cout<<"-------------------"<<endl;
    cout<<"Primer numero: "<<num1<<endl;
    cout<<"Segundo numero: "<<num2<<endl;

    return 0;
}

void intercambio(float *dir_num1,float *dir_num2){
    float aux;
    aux=*dir_num1;
    *dir_num1=*dir_num2;
    *dir_num2=aux;
}