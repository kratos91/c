/*
    Metodo de ordenamiento por insercion con punteros
*/

#include<iostream>
#include<stdlib.h>

using namespace std;

void pedirDatos();
void mostrarArreglo(int *,int);
void ordenarArreglo(int *,int);

int *ptrArreglo,nEle;

int main(){
    pedirDatos();
    ordenarArreglo(ptrArreglo,nEle);
    mostrarArreglo(ptrArreglo,nEle);

    //borrar arreglo
    delete[] ptrArreglo;
    return 0;
}

void pedirDatos(){
    cout<<"Ingrese el numero de elementos del arreglo: ";cin>>nEle;
    ptrArreglo=new int[nEle];

    for(int i=0;i<nEle;i++){
        cout<<"Ingrese el elemento["<<i<<"]: "; cin>>*(ptrArreglo+i);
    }
}

void mostrarArreglo(int *ptr,int elementos){
    cout<<"Mostrando elementos del arreglo..."<<endl;
    for(int i=0;i<elementos;i++){
        cout<<*(ptr+i)<<" ";
    }
    cout<<"\n";
}

void ordenarArreglo(int *ptr,int elementos){
    int pos,aux;
    //algoritmo de ordenamiento por inserción
    for(int i=0;i<elementos;i++){
        pos=i;
        aux=*(ptr+i);
        
        while ((pos>0) && (*(ptr+(pos-1))>aux)){
            *(ptr+pos)=*(ptr+(pos-1));
            pos--;
        }
        *(ptr+pos)=aux;
    }
}