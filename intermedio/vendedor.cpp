#include <iostream>
#include <iomanip>
#include "vendedor.h"

using namespace std;

Vendedor::Vendedor(){
    for(int i=0;i<12;i++)
        ventas[i]=0.0;
}

void Vendedor::obtieneVentasDelUsuario(){
    double montoVentas;
    for(int i=1;i<=12;i++){
        cout<<"Introduzca el monto de las ventas de un mes "<<i<<": ";
        cin>>montoVentas;
        estableceVentas(i,montoVentas);
    }
}

void Vendedor::estableceVentas(int mes,double monto){
    if(mes>=1 && mes<=12 && monto>0)
        ventas[mes-1]=monto;
    else
        cout<<"Mes o monto no valido"<<endl;
    
}

void Vendedor::imprimeVentasAnuales(){
    cout<<setprecision(2)
        <<setiosflags(ios::fixed | ios::showpoint)
        <<"\nEl total de las ventas anuales es: $"
        <<totalVentasAnuales()<<endl;
}

double Vendedor::totalVentasAnuales(){
    double total=0.0;
    for(int i=0;i<12;i++){
        total+=ventas[i];
    }

    return total;
}