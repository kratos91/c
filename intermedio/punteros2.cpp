/*Comprobar si un numero es par o impar con punteros*/
#include <iostream>
#include <stdlib.h>

using namespace std;

int main(){
    int num,*dir_num;
    dir_num=&num;
    cout<<"Ingrese un numero: "; cin>>num;
    
    if(*dir_num%2==0){
        cout<<"El numero es par"<<endl;
        cout<<"Direccion de mem: "<<dir_num<<endl;
    }else{
        cout<<"El numero es impar"<<endl;
        cout<<"Direccion de mem: "<<dir_num<<endl;
    }
    return 0;   
}