/*
    Hacer una función para almacenar N números en un arreglo dinamico,
    posteriormente en otra función buscar un número en particular.
*/
#include <iostream>
#include <stdlib.h>

using namespace std;

int nElementos,*ptrEle;
void pedirDatos();
void buscarElemento(int *,int);

int main(){
    pedirDatos();
    buscarElemento(ptrEle,nElementos);
    delete[] ptrEle;
    return 0;
}

void pedirDatos(){
    cout<<"Ingrese el tamaño del arreglo: ";
    cin>>nElementos;

    ptrEle=new int[nElementos];

    for(int i=0;i<nElementos;i++){
        cout<<"Ingrese un número para ["<<i<<"]: ";
        cin>>*(ptrEle+i);
    }
}

void buscarElemento(int *ptr,int nEle){
    int dato,i;
    bool bandera=false;

    cout<<"\n\nDigite un número a buscar: ";
    cin>>dato;
    i=0;

    while((i<nEle) && (bandera==false)){
        if(*(ptr+i)==dato){
            bandera=true;
        }
        i++;
    }

    if(bandera==false){
        cout<<"No se encontro el elemento "<<dato<<endl;
    }else{
        cout<<"Se encontro el elemento "<<dato<<" en el arreglo"<<endl;
    }
}