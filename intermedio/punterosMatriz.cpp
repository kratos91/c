/*
    Inicializar matriz con punteros
*/
#include <iostream>
#include <stdlib.h>

using namespace std;
void pedirDatos();
void mostrarDatos(int **,int,int);

int **ptrMatriz,nFilas,nCol;

int main(){
    pedirDatos();
    mostrarDatos(ptrMatriz,nFilas,nCol);

    //liberar la memoria
    for (int i = 0; i < nFilas; i++)
    {
        delete[] ptrMatriz[i];
    }
    
    delete[] ptrMatriz;
    
    return 0;
}

void pedirDatos(){
    cout<<"Numero de filas: "; cin>>nFilas;
    cout<<"Numero de columnas: "; cin>>nCol;

    ptrMatriz=new int*[nFilas]; //se reserva memoria para las filas
    for (int i = 0; i < nFilas; i++)
    {   
        ptrMatriz[i]=new int[nCol];// se reserva memoria para las columnas
    }

    //Digitando elementos de la matriz
    for(int i=0;i<nFilas;i++){
        for(int j=0;j<nCol;j++){
            cout<<"Introduce un numero["<<i<<"]["<<j<<"]: "; 
            cin>>*(*(ptrMatriz+i)+j);//ptrMatriz[i][j]
        }
    }
    
}

void mostrarDatos(int **ptrMa,int filas,int col){
    cout<<"Impriiendo matriz..."<<endl;

    for (int i = 0; i < filas; i++)
    {
        for (int j = 0; j < col; j++)
        {
            cout<<*(*(ptrMa+i)+j)<<" ";
        }   
        cout<<endl;
        
    }
    
}