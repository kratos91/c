//Punteros a estructuras
#include <iostream>
#include <stdlib.h>

using namespace std;

struct Persona
{
    char nombre[30];
    int edad;
};

void pedirDatos(Persona *ptrPe);
void mostrarDatos(Persona *ptrPe);

int main(){
    struct Persona persona,*ptrPersona;
    ptrPersona=&persona;
    pedirDatos(ptrPersona);
    mostrarDatos(ptrPersona);
    return 0;
}

void pedirDatos(Persona *ptrPe){
    cout<<"Digita tu nombre: "; cin.getline(ptrPe->nombre,30,'\n');
    cout<<"Digita tu edad: "; cin>>ptrPe->edad;
}

void mostrarDatos(Persona *ptrPe){
    cout<<"El nombre es "<<ptrPe->nombre<<endl;
    cout<<"La edad es "<<ptrPe->edad<<endl;
}