#include<iostream>
#include<stdlib.h>

using namespace std;

struct info_direccion{ 
    char direccion[30];
    char ciudad[20];
    char provincia[20];
};

struct empleado{
    char nombre[20];
    struct info_direccion dir_empleado;
    double salario;
};

int main(){
    struct empleado empleado[2];

    for(int i=0;i<2;i++){
        fflush(stdin);
        cout<<"Ingrese su nombre: "; cin>>empleado[i].nombre;
        cout<<"Ingrese su direccion: "; cin>>empleado[i].dir_empleado.direccion;
        cout<<"Ingrese su ciudad: "; cin>>empleado[i].dir_empleado.ciudad;
        cout<<"Ingrese su provincia: "; cin>>empleado[i].dir_empleado.provincia;
        cout<<"Ingrese su salario: "; cin>>empleado[i].salario;
    }

    for(int i=0;i<2;i++){
        cout<<"nombre: "<<empleado[i].nombre<<"\n";
        cout<<"direccion: "<<empleado[i].dir_empleado.direccion<<"\n";
        cout<<"ciudad: "<<empleado[i].dir_empleado.ciudad<<"\n";
        cout<<"provincia: "<<empleado[i].dir_empleado.provincia<<"\n";
        cout<<"salario: "<<empleado[i].salario<<"\n";
    }
    return 0;
}
