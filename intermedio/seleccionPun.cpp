/*
    Metodo de ordenamiento por seleccion con punteros
*/

#include<iostream>
#include<stdlib.h>

using namespace std;

void pedirDatos();
void mostrarArreglo(int *,int);
void ordenarArreglo(int *,int);

int *ptrArreglo,nEle;

int main(){
    pedirDatos();
    ordenarArreglo(ptrArreglo,nEle);
    mostrarArreglo(ptrArreglo,nEle);
    return 0;
}

void pedirDatos(){
    cout<<"Ingrese el numero de elementos del arreglo: ";cin>>nEle;
    ptrArreglo=new int[nEle];

    for(int i=0;i<nEle;i++){
        cout<<"Ingrese el elemento["<<i<<"]: "; cin>>*(ptrArreglo+i);
    }
}

void mostrarArreglo(int *ptr,int elementos){
    cout<<"Mostrando elementos del arreglo..."<<endl;
    for(int i=0;i<elementos;i++){
        cout<<*(ptr+i)<<" ";
    }
    cout<<"\n";
}

void ordenarArreglo(int *ptr,int elementos){
    int min,aux;
    //algoritmo de seleccion
    for (int i = 0; i < elementos; i++){
        min=i;
        //recorre los elementos del arreglo que esten adelante de i
        //hasta encontrar el minimo elemento,guarda la posicion
        //del elemento minimo y la intercambiow
        for(int j=i+1;j<elementos;j++){
            if(*(ptr+j)<*(ptr+min)){
                min=j;
            }
        }
        aux=*(ptr+i);
        *(ptr+i)=*(ptr+min);
        *(ptr+min)=aux;
    }
    
}