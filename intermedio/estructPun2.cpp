#include <iostream>
#include <stdlib.h>

using namespace std;

struct Alumno{
    char nombre[30];
    int edad;
    float promedio;
};
int alumnos;
void pedirDatos(Alumno *ptr,int);
void mostrarDatos(Alumno *ptr,int);

int main(){
    cout<<"Ingrese numero de alumnos: "; cin>>alumnos;
    struct Alumno *ptrArreglo=new Alumno[alumnos];
    pedirDatos(ptrArreglo,alumnos);
    mostrarDatos(ptrArreglo,alumnos);
    //borrando arreglo de estructuras
    delete[] ptrArreglo;
    return 0;
}

void pedirDatos(Alumno *ptr,int num){
    cout<<"Pidiendo datos de los alumnos"<<endl;
    for (int i = 0; i < num; i++){
        cout<<"Ingrese el nombre del alumno "<<i+1<<" : ";  cin>>((ptr+i)->nombre);
        cout<<"Ingrese edad del alumno "<<i+1<<" : "; cin>>((ptr+i)->edad);
        cout<<"Ingrese promedio del alumno "<<i+1<<" : "; cin>>((ptr+i)->promedio);
    }
}

void mostrarDatos(Alumno *ptr,int num){
    cout<<"Mostrando datos..."<<endl;
    for (int i = 0; i < num; i++){
        cout<<"Nombre del alumno "<<i+1<<" es: "<<(ptr+i)->nombre<<endl;
        cout<<"Edad del alumno "<<i+1<<" es: "<<(ptr+i)->edad<<endl;
        cout<<"Promedio del alumno "<<i+1<<" es: "<<(ptr+i)->promedio<<endl;
    }
    
}