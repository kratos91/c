/*
    Suma de dos matrices dinamicas
*/
#include <iostream>
#include <stdlib.h>

using namespace std;

void pedirDatos();
void sumarMatrices(int**,int**,int,int);
int **ptrMa1,**ptrMa2,nFil,nCol;

int main(){
    pedirDatos();
    sumarMatrices(ptrMa1,ptrMa2,nFil,nCol);
    //Muestra la matriz1 donde se guardo la suma
    cout<<"Sumando de las matrices: "<<endl;
    for(int i=0;i<nFil;i++){
        for(int j=0;j<nCol;j++){
            cout<<*(*(ptrMa1+i)+j)<<" ";
        }
        cout<<endl;
    }

    //liberar la memoria
    for (int i = 0; i < nFil; i++){
        delete[] ptrMa1[i];
    }

    for (int i = 0; i < nFil; i++){
            delete[] ptrMa2[i];
    }
    
    delete[] ptrMa2;
    delete[] ptrMa1;

    return 0;
}

void pedirDatos(){
    cout<<"Número de filas: "; cin>>nFil;
    cout<<"Número de columnas: "; cin>>nCol;

    //Reserva el espacio para la matriz 1
    ptrMa1=new int*[nFil];//reserva memoria para las filas
    for(int i=0;i<nFil;i++){
        ptrMa1[i]=new int[nCol]; //reserva memoria para las columnas
    }

    //Pide los elementos para la matriz 1
    cout<<"Ingrese los elementos de la matriz 1"<<endl;
    for(int i=0;i<nFil;i++){
        for(int j=0;j<nCol;j++){
            cout<<"Introduce el elemento ["<<i<<"]["<<j<<"]: "; cin>>*(*(ptrMa1+i)+j);
        }
    }


    //Reserva el espacio para la matriz 2
    ptrMa2=new int*[nFil];//reserva memoria para las filas
    for(int i=0;i<nFil;i++){
        ptrMa2[i]=new int[nCol]; //reserva memoria para las columnas
    }

    //Pide los elementos para la matriz 1
    cout<<"Ingrese los elementos de la matriz 2"<<endl;
    for(int i=0;i<nFil;i++){
        for(int j=0;j<nCol;j++){
            cout<<"Introduce el elemento ["<<i<<"]["<<j<<"]: "; cin>>*(*(ptrMa2+i)+j);
        }
    }
}

void sumarMatrices(int **ptr1,int **ptr2,int nFil,int nCol){
 
    for(int i=0;i<nFil;i++){
        for(int j=0;j<nCol;j++){
            *(*(ptr1+i)+j)+=*(*(ptr2+i))+j;
        }
    }
}