/*Determinar si un numero es primo o no con punteros*/
#include <iostream>
#include <stdlib.h>

using namespace std;

int main(){
    int num,*dir_num,cont=0;
    cout<<"Ingrese un numero: ";cin>>num;
    dir_num=&num;

    for(int i=1;i<*dir_num;i++){
        if(*dir_num%i==0){
            cont++;
        }
    }

    if(cont>2){
        cout<<"El numero "<<*dir_num<<" NO ES PRIMO"<<endl;
        cout<<"La direccion de mem "<<dir_num<<endl;
    }else
    {
        cout<<"El numero "<<*dir_num<<" ES PRIMO"<<endl;
        cout<<"La direccion de mem "<<dir_num<<endl;
    }
    
    return 0;
}