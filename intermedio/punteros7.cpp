#include <iostream>
#include <stdlib.h>

using namespace std; 

int hallarMax(int *,int);

int main(){
    const int nElementos=5;
    int numeros[nElementos]={6,2,14,1,2};

    cout<<"El mayor elemento es: "<<hallarMax(numeros,nElementos)<<endl;
    return 0;
}

int hallarMax(int *dir,int tam){
    int max=0;

    for(int i=0;i<tam;i++){
        if(*(dir+i)>max){
            max=*(dir+i);
        }
    }

    return max;
}