/*
    Pedir una matriz y crearla dinamicamente y con punteros hacer su transpuesta

    | 1 2 3 |      | 1 4 7 | 
    | 4 5 6 | -->  | 2 5 8 |
    | 7 8 9 |      | 3 6 9 |

    La transpuesta es aquella en que la columna i  era la fila i de la mtriz original
*/

#include <iostream>
#include <stdlib.h>

using namespace std;
int **ptrMa,nFil,nCol;

void pedirDatos();
void mostrarTrans(int**,int,int);
int main(){
    pedirDatos();
    mostrarTrans(ptrMa,nFil,nCol);
    //Borrando de la memmoria
    for(int i=0;i<nFil;i++){    
        delete[] ptrMa[i];
    }

    delete [] ptrMa;
    return 0;
}

void pedirDatos(){
    
    cout<<"Número de filas: "; cin>>nFil;
    cout<<"Número de columnas: "; cin>>nCol;

    //Reserva el espacio para la matriz 1
    ptrMa=new int*[nFil];//reserva memoria para las filas
    for(int i=0;i<nFil;i++){
        ptrMa[i]=new int[nCol]; //reserva memoria para las columnas
    }

    cout<<"Ingrese datos de la matriz"<<endl;
    for(int i=0;i<nFil;i++){
        for(int j=0;j<nCol;j++){
            cout<<"Ingrese ["<<i<<"]["<<j<<"]: "; cin>>*(*(ptrMa+i)+j);
        }
    }
}

void mostrarTrans(int **ptrM,int nF,int nC){
    cout<<"Imprimiendo original..."<<endl;
    for(int i=0;i<nF;i++){
        for(int j=0;j<nC;j++){
            cout<<*(*(ptrM+i)+j)<<" ";
        }
        cout<<endl;
    }

    cout<<"\nImprimiendo transpuesta..."<<endl;
    for(int i=0;i<nF;i++){
        for(int j=0;j<nC;j++){
            cout<<*(*(ptrM+j)+i)<<" ";
        }
        cout<<endl;
    }
}
