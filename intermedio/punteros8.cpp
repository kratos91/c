#include <stdio.h>

int main(){

    char *cadena="Hola esto es una cadena";
    int b[]={10,20,30,40};
    int *ptrB=b;
    int i;
    int desplazamiento;

    for(i=0;i<4;i++){
        printf("*(ptrB+%d)=%d\n",i,*(ptrB+i));
    }

    for(desplazamiento=0;desplazamiento<4;desplazamiento++){
        printf("*(b+%d)=%d\n",desplazamiento,*(b+desplazamiento));
    }
    printf("\n\n");
    for(;*cadena!='\0';cadena++){
        printf("%c",*cadena);
    }
    return 0;
}