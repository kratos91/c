#include <iostream>
#include <stdlib.h>

using namespace std;

int main(){
    int num=20,*dir_num;
    dir_num=&num;   
    cout<<"Numero: "<<*dir_num<<endl;
    cout<<"Direccion de memoria: "<<dir_num<<endl;
    return 0;
}