## Repositorio de prueba con ejemplos en C++

Un poco de código en C++ que se fue desarrollando como practica de este lenguaje

1. basico
    *algoritmos de ordenamiento (burbuja,shell,etc)
    *algoritmos de busqueda
2. intermedio
    *estructuras
    *archivos
    *punteros
    *matrices
3. avanzado
    *hilos
    *Sockets TCP (cliente y servidor)
    *Sockets TCP orientado a objeto (cliente y servidor)
4. estructuras de datos
    *pila
    *cola
    *arbol binario